'use strict';

var app = angular.module('app', ['ngRoute', 'ui.router', 'ngResource']);

app.controller('defaultCtrl', [
    '$scope', function($scope){

}]);
app.controller('HomeCtrl', [
    '$scope', function($scope){

}]);
app.controller('CarsCtrl', [
    '$scope', 'Cars',
    function($scope, Cars){
        Cars.query({carId: '_cars'}, function(data) {
            $scope.cars = data;
        });
}]);
app.controller('CarDetailCtrl', [
    '$scope', '$stateParams', 'Cars',
    function($scope, $stateParams, Cars){
        $scope.carId = $stateParams.carId;

        Cars.get({carId: $stateParams.carId}, function(data) {
            $scope.car = data;
            $scope.mainImgUrl = data.images[0];

            $scope.setImage = function(imageUrl) {
                $scope.mainImgUrl = imageUrl;
            }
        });
}]);
app.controller('AboutCtrl', [
    '$scope',
    function($scope){
        $scope.items = [
            { id: 0, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 1, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 2, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 3, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 0, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 1, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 2, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 3, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 0, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 1, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 2, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' },
            { id: 3, caption: 'Pellentesque habitant', text: 'Pellentesque habitant morbi tristique' }
        ]
}]);
app.controller('ContactCtrl', [
    '$scope',
    function($scope){
        $scope.sendMail = function($event) {
            $event.preventDefault();
            console.log("+")
        }
}]);

