'use strict';

app.factory('Cars', [
    '$resource', function($resource) {
        return $resource('app/json/:carId.:format', {
            carId: '_cars',
            format: 'json'
        }, {
            // action: {method: <?>, params: <?>, isArray: <?>, ...}
            update: {method: 'PUT', params: {carId: '@car'}, isArray: true}
        });
    }
]);