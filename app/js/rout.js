'use strict';
app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            abstract: true,
            templateUrl: '/templates/default.html'
        })
        .state('home.default', {
            url: '/',
            templateUrl: '/templates/home.html',
            controller: 'HomeCtrl'
        })
        .state('home.about', {
            url: '/about',
            templateUrl: '/templates/about.html',
            controller: 'AboutCtrl'
        })
        .state('home.cars', {
            url: '/cars',
            templateUrl: '/templates/cars.html',
            controller: 'CarsCtrl'
        })
        .state('home.contact', {
            url: '/contact',
            templateUrl: '/templates/contact.html',
            controller: 'ContactCtrl'
        })
        .state('home.detail', {
            url: '^/:carId',
            views: {
                '': {
                    templateUrl: '/templates/car-detail.html',
                    controller: 'CarDetailCtrl'
                }
            }
        })
});
