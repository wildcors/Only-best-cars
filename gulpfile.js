var gulp = require('gulp'),
    runSequence = require('run-sequence').use(gulp),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass'),
    del = require('del'),
    browserSync = require('browser-sync'),
    ngAnnotate = require('gulp-ng-annotate');
    

gulp.task('js', function() {
    return gulp.src(['./app/js/**/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('./js'))
})

gulp.task('img-min', function () {
   return gulp.src('./app/images/**/*')
       .pipe(imagemin({ optimizationLevel: 4, progressive: true}))
       .pipe(gulp.dest('./images'))
});

gulp.task('sass', function () {
  return gulp.src('./app/styles/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('clean', function() {
    return del(['./css/*', './js/*', './images/*']);
});

gulp.task('browser-sync', function () {
   var files = [
      './*.html',
      './css/**/*.css',
      './images/**/*.png',
      './js/**/*.js'
   ];

   browserSync.init(files, {
      server: {
         baseDir: './'
      }
   });
});

gulp.task('watch', ['browser-sync'], function () {
  gulp.watch('app/images/**/**.*', ['img-min']);
  gulp.watch('app/js/*.js', ['js']);
  gulp.watch('app/styles/*.scss', ['sass']);
})

gulp.task('default', ['clean'], function () {
   runSequence('img-min', 'sass', 'js', 'watch');
});
